## Not Actively Maintained. Should Still Work. Merge Requests Accepted.

**I HAVE NO INTEREST IN MAINTAINING THIS TOOL.** I stopped playing League of Legends when [Riot announced they were developing wine-incompatible anti-cheat](https://na.leagueoflegends.com/en-us/news/dev/dev-null-anti-cheat-kernel-driver/)). This should work as long as there are no breaking changes to [sysctl](https://linux.die.net/man/8/sysctl) or [zenity](https://linux.die.net/man/1/zenity), which are both fairly stable. **I will merge any MR after verifying it is not malicious.**

# README

Descriptions of what this tool does are in the dialogs in the scripts themselves (they're under 150 lines total, just read them). Maybe I'll update this README later.

There is a funny requirement for legibility here, because they are intended to be embedded into the Lutris League of Legends installer. The installer can only accept two formats:

1. Everything in a double-quoted, one-line string, using `\n` for line breaks. This makes them unreadable in the script and requires someone to run the installer and write the files first.. not great for transparency. This is what I did in the first version of the scripts, and as a compromise, I didn't execute the scripts during install, to give people a chance to read them before they're run.
2. Use a single-quoted, multi-line string, using line breaks for line breaks. This makes the script way more legible.. but when Lutris writes a file expressed in way, it strips leading whitespace on every line (according to the yaml spec). Thus, **in order to preserve legibility of the files on disk, these scripts must be readable without any indentation.**

There's another format option in the yaml spec that would have neither of these downsides, but unfortunately the lutris.net parser does not support it (it gets converted to the double quoted type when saved).

The version here is licensed GPLv3 or, at your option, any later version.
